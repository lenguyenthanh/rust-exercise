use structopt::StructOpt;

mod config;
use config::*;

fn main() -> Result<(), config::ConfigError>{
    let args = CmdlineArgs::from_args();
    println!("{:?}", args);

    let config = Config::new(args.root, args.name, args.email);
    save_config(&config)
}

#[derive(Debug, StructOpt)]
/// I am a program and I work, just pass `-h`
struct CmdlineArgs {
    #[structopt(long, short, default_value = "./dadmin")]
    root: String,

    #[structopt(long = "name", short = "n")]
    name: String,

    #[structopt(short, long)]
    email: String,
}

