# FP Rust

A command line tool for creating/updating configuration

## Requirement

### Init

- Read input
- Validate input
- Save/update to file
- Support Toml then other formats
- Read it's own config to know where to save data.

## Structure

- cli
- core
  - init
  - update
  - login
  - clone
- service
  - git
  - dir
  - file

examples:

    servo

    ripgrep

    clippy

    the rust compiler and standard library

    rustfmt

    piston

    rocket
