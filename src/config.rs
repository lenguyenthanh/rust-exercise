use serde::{Deserialize, Serialize};

use thiserror::Error;
use toml::ser::Error as TomlError;

#[path = "fs.rs"]
mod fs;
use fs::*;

#[derive(Serialize, Deserialize, Debug)]
pub struct Config {
    root: String,
    name: String,
    email: String,
}

impl Config {
    pub fn new(root: String, name: String, email: String) -> Config {
        Config { root, name, email }
    }
}

pub fn save_config(config: &Config) -> Result<(), ConfigError> {
    let content = toml::to_string(config)?; //.map_err(|e| ConfigError::FormatError(e))?;
    write_to_file("config.toml", content.as_str()).map_err(|e| ConfigError::IOError(e))
}

#[derive(Error, Debug)]
pub enum ConfigError {
    #[error("Format error {0:?}")]
    FormatError(#[from] TomlError),
    #[error("IO error {0:?}")]
    IOError(#[from] FileError),
}

pub trait ConfigService {
    fn save(config: &Config) -> Result<(), ConfigError>;
    fn fetch() -> Result<Config, ConfigError>;
}
