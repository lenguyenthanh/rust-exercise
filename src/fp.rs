use std::ops::Mul;

fn main() {
    let p = Rectangle { x: 3, y: 4 };
    let three = 3;
    println!("foo3 : {:?}", foo(three));
    println!("fooo : {:?}", fooo(&three));
    println!("bar: {:?}", bar(foo, 4));
    println!("bar1: {:?}", bar1(foo, 4));

    println!("area {:?}", area(&p));
    println!("area {:?}", area(&p));

    let _xs = (0..10).map(|x| x * x);
    let s = (0..10)
        .map(|x| x * x)
        .inspect(|y| print!("{},", y))
        .fold(0, |x, y| x + y);
    println!("{}", s);

    // if expression
    let _ix = if true { 1 } else { 2 };
    dbg!(Rectangle { x: 5, y: 11 } * Rectangle { x: 2, y: 4 });
    let r1 = Rectangle { x: 3, y: 5 };

    let x = {
        println!("Side effect");
        1 + 2
    };
    let y = || {
        println!("Side effect");
        1 + 2
    };

    println!("{:?}, {:?}", x, y());
}

fn foo<T>(x: T) -> T
where
    T: Mul<Output = T> + Clone,
{
    x.clone() * x
}

fn bar<F, T>(f: F, t: T) -> T
where
    F: Fn(T) -> T,
{
    f(t)
}

fn bar1<T>(f: impl Fn(T) -> T, t: T) -> T {
    f(t)
}

#[derive(Debug)]
struct Rectangle<T> {
    x: T,
    y: T,
}

fn area<'a, T>(rec: &'a Rectangle<T>) -> T
where
    &'a T: Mul<Output = T>,
{
    &rec.x * &rec.y
}

fn fooo<'a, T>(x: &'a T) -> T
where
    &'a T: Mul<Output = T>,
{
    x * x
}

impl<T> Mul for Rectangle<T>
where
    T: Mul<Output = T>,
{
    type Output = Rectangle<T>;
    fn mul(self, rhs: Rectangle<T>) -> Rectangle<T> {
        Rectangle {
            x: self.x * rhs.x,
            y: self.y * rhs.y,
        }
    }
}
