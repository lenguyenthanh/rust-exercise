use std::fs::File;
use std::io::{BufReader, BufWriter, Read, Write};
use std::path::Path;

use thiserror::Error;

pub fn read_file<P: AsRef<Path>>(path: P) -> Result<String, FileError> {
    let path = path.as_ref();

    let file = File::open(path).map_err(|_| FileError::AccessError)?;
    let mut file = BufReader::new(file);

    let mut result = String::new();
    file.read_to_string(&mut result)
        .map_err(|_| FileError::ReadError)?;

    Ok(result)
}

pub fn write_to_file<P: AsRef<Path>>(path: P, content: &str) -> Result<(), FileError> {
    let path = path.as_ref();

    let file = File::create(path).map_err(|_| FileError::AccessError)?;
    let mut file = BufWriter::new(file);

    file.write_all(content.as_bytes())
        .map_err(|_| FileError::WriteError)?;

    Ok(())
}

#[derive(Error, Debug)]
pub enum FileError {
    #[error("Could no create/open file:")]
    AccessError,
    #[error("Could no write to file:")]
    WriteError,
    #[error("Could no write to file:")]
    ReadError,
}
